CC       = gcc
LIBS     = -lz
BUILDNO	 =$(shell cat build)
BIN	 =$(shell ls balong_flash 2> /dev/null)
OBJFILES =$(shell ls balong_flash.o 2> /dev/null)
CFLAGS   = -O2 -Wunused -Wno-unused-result -D BUILDNO=$(BUILDNO) -DLOCALE_DIR='"$(LOCALE_DIR)"' -D_7ZIP_ST $(LIBS) 
INSTDIR  = /usr/bin
INSTBIN	 = $(shell ls $(INSTDIR)/balong_flash 2> /dev/null)
.PHONY: all clean

all:    balong_flash $(MOFILES)

clean: 
	@if [ $(OBJFILES) ] && [ -d $(LOCALE_DIR) ];\
	then \
		rm *.o 2> /dev/null;\
		rm balong_flash 2> /dev/null;\
		echo "Build files are clean";\
	else \
		echo 'Cannot cleaning build files, run "make balong_flash" or just "make" first!';\
		exit 1;\
	fi

balong_flash: balong_flash.o hdlcio_linux.o ptable.o flasher.o util.o signver.o
	@gcc $^ -o $@ $(LIBS)
	@echo Current balong_flash buid: $(BUILDNO)
	@echo Build success. now run make install as root.

install:
	@if ! [ "$(shell id -u)" = 0 ]; then \
		echo "You are not root, please run make install as root";\
		exit 1;\
	fi
	@if [ $(BIN) ] && [ -d $(INSTDIR) ]; then \
		cp balong_flash $(INSTDIR);\
		chmod a+x $(INSTDIR)/balong_flash;\
		chmod og-w $(INSTDIR)/balong_flash;\
		echo "Installed balong_flash in $(INSTDIR)";\
	else \
		echo 'Sorry, please run "make balong_flash" or just "make" before "make install"';\
		exit 1;\
	fi
	
uninstall: clean
	@if ! [ "$(shell id -u)" = 0 ]; then \
		echo "You are not root, please run make uninstall as root";\
		exit 1;\
	fi
	@if [ $(INSTBIN) ] && [ -d $(INSTDIR) ]; \
	then \
		rm $(INSTDIR)/balong_flash 2> /dev/null;\
		echo "Success uninstalling balong_flash";\
	else \
		echo "Sorry, $(INSTDIR)/balong_flash does not installed";\
		exit 1;\
	fi
